package INF101.lab2;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class Fridge implements IFridge {

    ArrayList<FridgeItem> itemList = new ArrayList<>();
    int capacity = 20;


    @Override
    public int nItemsInFridge() {
        return itemList.size();
    }

    @Override


    public int totalSize() {
        return capacity;
    }

    @Override
    public boolean placeIn(FridgeItem item) {
        if (capacity > itemList.size()) {
            itemList.add(item);
            return true;
        } else {
            return false;
        }
    }

    @Override
    public void takeOut(FridgeItem item) {
        if (!itemList.contains(item)) {

            throw new NoSuchElementException();
        } else {
            itemList.remove(item);
        }
    }

    @Override
    public void emptyFridge() {
        this.itemList.clear();
    }

    @Override
    public List<FridgeItem> removeExpiredFood() {
        ArrayList<FridgeItem> expiredFood = new ArrayList<>();


        for(FridgeItem theItem : itemList) {
            try {
                if (theItem.hasExpired()) {
                    //itemList.remove(theItem);
                    expiredFood.add(theItem);
                } else {
                    continue;
                }
            } catch (IllegalStateException e) {      //fikk hjelp av Sebastian Helgesen til exeption statetmentet
                expiredFood.add(theItem);
            }
        }
        for(FridgeItem expiredItem : expiredFood){
            this.takeOut(expiredItem);
        }
        return expiredFood;

    }

}